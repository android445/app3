package com.naranya.app3.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.naranya.app3.R
import com.naranya.app3.model.Gas


class GasAdapter(
    private val context: Context,
    private val dataset: List<Gas>
) : RecyclerView.Adapter<GasAdapter.ItemViewHolder>() {

    class ItemViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.item_price)
        val textView1: TextView = view.findViewById(R.id.item_title)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = context.resources.getString(item.stringResourcePrice)
        holder.textView1.text = context.resources.getString(item.stringResourceOil)
        holder.itemView.setOnClickListener {
            Toast.makeText(
                context,
                context.resources.getString(item.stringResourcePrice) +" "+context.resources.getString(item.stringResourceOil),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}
