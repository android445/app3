package com.naranya.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.naranya.app3.adapter.GasAdapter
import com.naranya.app3.data.datas

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myDataset = datas().loadGas()
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = GasAdapter(this, myDataset)
        recyclerView.setHasFixedSize(true)
    }

}