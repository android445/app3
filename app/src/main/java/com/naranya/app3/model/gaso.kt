package com.naranya.app3.model

import androidx.annotation.StringRes

data class Gas(
    @StringRes val stringResourcePrice: Int,
    @StringRes val stringResourceOil: Int,

    )