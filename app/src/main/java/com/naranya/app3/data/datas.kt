package com.naranya.app3.data

import com.naranya.app3.R
import com.naranya.app3.model.Gas

class datas {
    fun loadGas(): List<Gas> {
        return listOf<Gas>(
            Gas(R.string.price1,R.string.oil1),
            Gas(R.string.price2,R.string.oil2),
            Gas(R.string.price3,R.string.oil3),
            Gas(R.string.price4,R.string.oil4),
            Gas(R.string.price5,R.string.oil5),
            Gas(R.string.price6,R.string.oil6),
            Gas(R.string.price7,R.string.oil7),
            Gas(R.string.price8, R.string.oil8),
            Gas(R.string.price9,R.string.oil9),
        )
    }

}